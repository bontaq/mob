# mob 
A tool to facilitate mob programming

## Dev Setup
Install the Haskell Platform (Haskell Compiler, Stack, and Cabal)

`https://www.haskell.org/platform/`


Note if you are using fish shell, you may wish to install this add on to automatically handle adding haskell things to your `PATH`

`https://github.com/halostatue/fish-haskell`


## Running tests

`cabal exec runhaskell tests/TestHelloWorld.hs`