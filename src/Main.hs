module Main where

main :: IO ()
main = do
  putStrLn $ helloWorld "World"

helloWorld :: [Char] -> [Char]
helloWorld name = "Hello, " ++ name
