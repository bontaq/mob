module Main where

import Test.Tasty
import Test.Tasty.HUnit
import System.Exit
import Control.Exception
import HelloWorld

main = defaultMain $
  testCase "Hello Bob" $ do
    assertEqual "Can say hello to Bob" "Hello, Bob" (helloWorld "Bob")
